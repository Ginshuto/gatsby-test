// const fetch / axios
// dotenv

// MAILCHIMP: 
//     - GATSBY_MAILCHIMP_API
//     - MAILCHIMP_ID_LIST

// STRIPE : STRIPE_API

//     - event.body
//     - Contrôle sur req
//     - fetch sur API MAILCHIMP
//     - en fonction rep => callback

const fetch = require("node-fetch");
Headers = fetch.Headers;
require('dotenv').config();

const mailChimpApi = process.env.GATSBY_API_KEY;
const mailChimpListID = process.env.MAILCHIMP_ID_LIST;


exports.handler = function (event, context, callback) {
    fetch(`https://us8.api.mailchimp.com/3.0/lists/${mailChimpListID}/members`, {
        method: 'POST',
        headers: new Headers({
            'Authorization': 'Basic ' + Buffer.from("ginshuto:" + mailChimpApi).toString('base64'),
            'Content-Type': 'application/json'
        }),
        body: JSON.stringify({
            "email_address": event.queryStringParameters.email,
            "status": "subscribed"
        }),
    }).then(() => {
        callback(
            null, {
                statusCode: 200,
                body: "Vous avez souscrit à la newsletter !",
                redirect: "/newsletter"
            }
        )
    }).catch((error) => {
        callback(
            null, {
                statusCode: 400,
                body: "Erreur :" + error,
            }
        )
    })
}