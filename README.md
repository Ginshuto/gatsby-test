[Lien vers l'application Netlify](https://clever-mclean-281753.netlify.app/)

## Features 
* LocalStorage ne fonctionne pas sur netlify, uniquement en local.
* Formulaire de contact,newsletter, ainsi que inscription/connexion fonctionnent sur netlify.
* Redirection success/cancel de stripe vers l'application netlify
* Début du Slider sur la page d'accueil