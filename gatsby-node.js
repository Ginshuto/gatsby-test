
exports.createPages = async function({ actions, graphql }) {
    const products = await graphql(`
        query myQuery {
          allMarkdownRemark(filter: {fileAbsolutePath: {regex: "/products/"}}) {
            edges {
              node {
                frontmatter {
                  path
                }
              }
            }
          }
        }
    `)
    products.data.allMarkdownRemark.edges.forEach(edge => {
      const path = edge.node.frontmatter.path;
      actions.createPage({
          path: `product/${path}`,
          component: require.resolve('./src/templates/productTemplate.js'),
          context:{url: path}
        }
      )
    })

    const articles = await graphql(`
        query myQuery {
        allMarkdownRemark(filter: {fileAbsolutePath: {regex: "/articles/"}}) {
            edges {
            node {
                frontmatter {
                path
                }
            }
            }
        }
        }
    `)

    articles.data.allMarkdownRemark.edges.forEach(edge => {
      const path = edge.node.frontmatter.path;
      actions.createPage({
        path: `article/${path}`,
        component: require.resolve('./src/templates/articleTemplate.js'),
        context: {url: path}
      }
    )
    })
  }