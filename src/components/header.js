import { Link } from "gatsby"
import React from "react"
import IdentityModal, {
  useIdentityContext,
} from "react-netlify-identity-widget"
import "react-netlify-identity-widget/styles.css"

const Header = ({ siteTitle }) => {
  const identity = useIdentityContext()
  const [dialog, setDialog] = React.useState(false)
  const isLoggedIn = identity && identity.isLoggedIn
  // Si is loggedIn false, on peut pas accéder au panier, ni ajouter un objet au panier
  return (
    <header>
      <nav>
        <ul>
          <li className="nav__item">
            <Link to="/">Home</Link>
          </li>
          <li className="nav__item">
            <Link to="/products">Eshop</Link>
          </li>
          <li className="nav__item">
            <Link to="/articles">Blog</Link>
          </li>
          <li className="nav__item">
            <Link to="/contact">Contact</Link>
          </li>
          {isLoggedIn ? (
            <div>
              <li className="nav__item">
                <Link to="/account">My Account</Link>
              </li>
              <li className="nav__item">
                <Link to="/cart">Cart</Link>
              </li>
            </div>
          ) : (
            <div></div>
          )}
        </ul>
      </nav>
      <div className="toolbar">
        <button className="btn" onClick={() => setDialog(true)}>
          {isLoggedIn ? `Log out` : "Login"}
        </button>
      </div>
      <IdentityModal
        showDialog={dialog}
        onCloseDialog={() => setDialog(false)}
      />
    </header>
  )
}

export default Header
