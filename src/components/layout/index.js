import React from 'react'
import Header from '../header'
import Footer from '../footer/footer'
import SEO from '../seo'

const Index = (props) => {
    return (
        <div>
            <Header/>
            <main>
                <SEO title={props.name} />
                <h1>{props.name}</h1>
                {props.children}
            </main>
            <Footer/>
        </div>
    )
}

export default Index