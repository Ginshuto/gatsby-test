import React from 'react'
import { Link } from 'gatsby'

const Footer = () => {
    return (
        <footer>
            <Link to="/newsletter">Newsletter Subscription</Link>
        </footer>
        )
}

export default Footer
