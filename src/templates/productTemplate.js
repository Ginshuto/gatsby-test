import React, { useState } from "react"
import Layout from "../components/layout"
import { graphql, Link } from "gatsby"
import { useIdentityContext } from "react-netlify-identity-widget"

export const query = graphql`
  query($url: String!) {
    markdownRemark(frontmatter: { path: { eq: $url } }) {
      frontmatter {
        title
        price
        path
        description
        image
        sku
        category
        attributes
      }
    }
  }
`
export default ({ data }) => {
  const windowGlobal = typeof window !== 'undefined' && window
  const identity = useIdentityContext()
  const isLoggedIn = identity && identity.isLoggedIn
  const [quantity, setQuantity] = useState(1)
  function addToCart() {
    var object = data.markdownRemark.frontmatter
    object.quantity = quantity
    console.log(object)
    windowGlobal.localStorage.setItem(
      `${data.markdownRemark.frontmatter.sku}`,
      JSON.stringify(object)
    )
  }

  function incrementQuantity() {
    quantity++
  }
  return (
    <Layout name={data.markdownRemark.frontmatter.title}>
      <Link to="/products">Back to products list</Link>
      <p>{data.markdownRemark.frontmatter.price} €</p>
      <p>{data.markdownRemark.frontmatter.description}</p>
      <img src={data.markdownRemark.frontmatter.image} />
      <div className="product__quantity">
        Quantity :
        <button onClick={() => setQuantity(quantity - 1)}>-</button>
        {quantity}
        <button onClick={() => setQuantity(quantity + 1)}>+</button>
      </div>
      {isLoggedIn ? (
        <button className="product__button">Add to Cart</button>
      ) : (
        <button
          className="product__button__forbidden"
          title="Login to add the product to the cart."
          onClick={addToCart}
        >
          Add to Cart
        </button>
      )}
    </Layout>
  )
}
