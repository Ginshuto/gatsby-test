import React from "react"
import Layout from "../components/layout"
import {graphql} from "gatsby"

export const query = graphql`
    query($url: String!) {
        markdownRemark(frontmatter: {path: {eq:$url}}) {
            frontmatter {
                title
            }
        }
    }
`
export default ({data}) => {
    return (
      <Layout name={data.markdownRemark.frontmatter.title}>
        <h1>
          {data.markdownRemark.frontmatter.title}
        </h1>
      </Layout>
    )
}