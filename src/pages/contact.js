import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const Contact = () => (
  <Layout name="Contact">
    <form
      name="contact"
      action='javascript:alert("Your message has been delivered.")'
      method="POST"
      data-netlify="true"
    >
      <div>
        <label>Your Name:</label>
        <input type="text" id="name" name="name" />
      </div>
      <div>
        <label>Your Email:</label>
        <input id="email" type="email" name="email" />
      </div>
      <div>
        <label>Message:</label>
        <textarea id="message" name="message"></textarea>
      </div>
      <div>
        <input type="hidden" name="form-name" value="contact" />
        <input type="submit" value="envoyer" />
      </div>
    </form>
    <Link to="/">Go back to the homepage</Link>
  </Layout>
)

export default Contact
