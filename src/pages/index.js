import React from "react"
import Slider from "react-slick"
import { graphql, Link } from "gatsby"
import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"

export const query = graphql`
  query {
    allMarkdownRemark(filter: { fileAbsolutePath: { regex: "/products/" } }) {
      edges {
        node {
          frontmatter {
            title
            price
            description
            image
            path
          }
        }
      }
    }
  }
`

export default ({ data }) => {
  var settings = {
    dots: true,
    infinite: true,
    autoplay: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplaySpeed: 2000,
      cssEase: "linear"
  }

  return (
    <Layout>
      <SEO title="Home" />
      <h1>Hi people</h1>
      <p>Welcome to your new Gatsby site.</p>
      <p>Now go build something great.</p>
      <Slider {...settings} className="products__slider">
        {data.allMarkdownRemark.edges.map(({ node }, index) => (
          <div className="products__item">
            <Link to={"/product/" + node.frontmatter.path}>
              <img src={node.frontmatter.image} width="300"/>
            </Link>
          </div>
        ))}
      </Slider>
    </Layout>
  )
}
