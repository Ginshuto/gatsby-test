import React from "react"
import Layout from "../../../components/layout"
import SEO from "../../../components/seo"

export default function index() {
  return (
    <Layout name="Cancel">
      <div>
        <p>Your purchase was cancelled.</p>
      </div>
    </Layout>
  )
}
