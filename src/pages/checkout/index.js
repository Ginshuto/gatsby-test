import React from "react"
import { loadStripe } from "@stripe/stripe-js"

const Index = () => {
  const stripePromise = loadStripe("pk_test_nn7tLZvXW7Qzq3h6idD1YnwY00A91LUWZU")

  const checkButton = async e => {
    e.preventDefault()
    const stripe = await stripePromise
    stripe
      .redirectToCheckout({
        items: [
          // Replace with the ID of your SKU
          { sku: "sku_HMPkKXXyz8qWwW", quantity: 1 },
        ],
        successUrl: "http://localhost:8000/checkout/success",
        cancelUrl: "http://localhost:8000/checkout/cancel",
      })
      .then(function (result) {
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer
        // using `result.error.message`.
      })
  }

  return (
    <div>
      <button onClick={checkButton}>Payer</button>
    </div>
  )
}

export default Index
