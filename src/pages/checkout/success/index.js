import React from "react"
import Layout from "../../../components/layout"
import SEO from "../../../components/seo"

export default function index() {
  return (
    <Layout name="Success !">
      <div>
        <p>Thank you for your purchase !</p>
      </div>
    </Layout>
  )
}
