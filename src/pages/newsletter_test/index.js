import React, {useState} from "react"
import fetch from "node-fetch";
import Layout from "../../components/layout"


const Index = () => {
    const [inputForm, setInputForm] = useState({
        email: ""
    });

    const handleChange = (e) =>{
        setInputForm({
            [e.target.name] : e.target.value
        })
    }

    const submitForm = (e) => {
        e.preventDefault();
        console.log(inputForm);
        fetch(`https://clever-mclean-281753.netlify.app/.netlify/functions/hello`,{
            method: 'POST',
            body: inputForm
        })
    }

    return (
        <Layout>
        <div>
            <form onSubmit={submitForm}>
                <label htmlFor="email">Email :</label>
                <input type="email" name="email" value={inputForm.email} onChange={handleChange} />
                <button type="submit">SUBMIT</button>
            </form>
        </div>
        </Layout>
    );
}

export default Index;
