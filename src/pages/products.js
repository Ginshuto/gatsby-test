import React, { useState } from "react"
import { graphql, Link } from "gatsby"
import Layout from "../components/layout"

export const query = graphql`
  query {
    allMarkdownRemark(filter: { fileAbsolutePath: { regex: "/products/" } }) {
      edges {
        node {
          frontmatter {
            title
            price
            description
            image
            path
            category
            attributes
          }
        }
      }
    }
  }
`

export default ({ data }) => {
  // Categories
  const categoriesArray = []
  for (let i = 0; i < data.allMarkdownRemark.edges.length; i++) {
    for (
      let j = 0;
      j < data.allMarkdownRemark.edges[i].node.frontmatter.category.length;
      j++
    ) {
      categoriesArray.push(
        data.allMarkdownRemark.edges[i].node.frontmatter.category[j]
      )
    }
  }
  var categoriesSet = new Set(categoriesArray)
  const categories = [...categoriesSet]

  // Attributes
  const attributesArray = []
  for (let i = 0; i < data.allMarkdownRemark.edges.length; i++) {
    for (
      let j = 0;
      j < data.allMarkdownRemark.edges[i].node.frontmatter.attributes.length;
      j++
    ) {
      attributesArray.push(
        data.allMarkdownRemark.edges[i].node.frontmatter.attributes[j]
      )
    }
  }
  var attributesSet = new Set(attributesArray)
  const attributes = [...attributesSet]

  // Pour chaque checkbox, useState true/false, quand on click setState
  // {variable vraie ? () : ()}

  const [productsData, setData] = useState(data.allMarkdownRemark.edges)
  const [previousData, setPrevious] = useState(productsData)
  const [checkState, setCheckState] = useState(false)
  const [filterCategory, setFilterCategory] = useState([])
  //if inside the array, remove it

  const checkboxVerification = (value, type) => {
    if (filterCategory.includes(value)) {
      for (let i = 0; i < filterCategory.length; i++) {
        if (filterCategory[i] === value) {
          filterCategory.splice(i, 1)
          i--
        }
      }
    } else {
      setFilterCategory([...filterCategory, value])
    }
    // if (checkState) {
    //   setData(previousData)
    //   setCheckState(!checkState)
    // } else {
    // setPrevious(productsData)

    const array = []
    productsData.forEach(element => {
      filterCategory.forEach(elem => {
        if (element.node.frontmatter.category.includes(elem)) {
          array.push(element)
        }
      })
    })
    console.log(array)
    // setData(array)

    const productsDataFiltered = productsData.filter(elem => {
      if (type == "category") {
        return elem.node.frontmatter.category.includes(value)
        // return filterCategory.forEach(item => {
        //   return elem.node.frontmatter.category.includes(item)
        // })
      } else {
        return elem.node.frontmatter.attributes.includes(value)
      }
    })
    console.log(productsDataFiltered)
    setData(productsDataFiltered)
    setCheckState(!checkState)
  }

  return (
    <Layout name="Products">
      <div className="category__filters">
        <h2>Category filters :</h2>
        {categories.map(filter => (
          <div className="category__filter">
            <input
              type="checkbox"
              defaultChecked={false}
              onClick={() => checkboxVerification(filter, "category")}
            />
            <label>{filter}</label>
          </div>
        ))}
      </div>
      <div className="attributes__filters">
        <h2>Attributes filters :</h2>
        {attributes.map(filter => (
          <div className="attributes__filter">
            <input
              type="checkbox"
              defaultChecked={false}
              onClick={() => checkboxVerification(filter, "attribute")}
            />
            <label>{filter}</label>
          </div>
        ))}
      </div>
      <div className="products__grid">
        {productsData.map(({ node }, index) => (
          <div className="products__item">
            <h3>{node.frontmatter.title}</h3>
            <p>{node.frontmatter.price} €</p>
            <p>{node.frontmatter.description}</p>
            <Link to={"/product/" + node.frontmatter.path}>
              <img src={node.frontmatter.image} />
            </Link>
            <ul className="category__list">
              {node.frontmatter.category.map(cat => (
                <li className="category__item">{cat}</li>
              ))}
            </ul>
            <ul className="attributes__list">
              {node.frontmatter.attributes.map(attr => (
                <li className="attributes__item">{attr}</li>
              ))}
            </ul>
            <Link to={"/product/" + node.frontmatter.path}>
              Product details
            </Link>
          </div>
        ))}
      </div>
    </Layout>
  )
}
