import React from "react"
import Layout from "../../components/layout"
import IdentityModal, {
  useIdentityContext,
} from "react-netlify-identity-widget"
import SEO from "../../components/seo"
import "react-netlify-identity-widget/styles.css"

const Account = () => {
  const identity = useIdentityContext()
  const [dialog, setDialog] = React.useState(false)
  // const name =
  //   (identity &&
  //     identity.user &&
  //     identity.user.user_metadata &&
  //     identity.user.user_metadata.full_name) ||
  //   "NoName"
  const isLoggedIn = identity && identity.isLoggedIn
  console.log(identity)
  return (
    <Layout name="Account">
      <h2>Account Informations :</h2>
      {isLoggedIn ? (
      <ul>
        <li>Full Name : {identity.user.user_metadata.full_name}</li>
        <li>Your Email : {identity.user.email}</li>
        <li>Created at : {identity.user.created_at}</li>
        <button onClick={()=>identity.requestPasswordRecovery}>Password Recovery</button>
      </ul>
      ):(<ul></ul>)}
    </Layout>
  )
}

export default Account
