import React from "react"
import { graphql, Link } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"

export const query = graphql`
  query {
    allMarkdownRemark(filter: { fileAbsolutePath: { regex: "/articles/" } }) {
      edges {
        node {
          frontmatter {
            title
            path
          }
        }
      }
    }
  }
`

export default ({ data }) => {
  return (
    <Layout name="Blog">
      <div className="articles__grid">
        {data.allMarkdownRemark.edges.map(({ node }, index) => (
          <div className="articles__item">
            <h1>{node.frontmatter.title}</h1>
            <Link to={"/article/"+node.frontmatter.path}>See the article</Link>
          </div>
        ))}
      </div>
    </Layout>
  )
}
