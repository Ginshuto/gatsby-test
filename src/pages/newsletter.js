import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const Newsletter = () => (
  <Layout name="Newsletter">
    <form name="signup" method="get" action="https://clever-mclean-281753.netlify.app/.netlify/functions/signup" netlify>
      <div className="field first">
        <label htmlFor="email">Email :</label>
        <input type="email" name="email"id="email" />
      </div>
      <button type="submit">Subscribe</button>
    </form>
  </Layout>
)

export default Newsletter
