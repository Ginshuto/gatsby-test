import React, { useState } from "react"
import { loadStripe } from "@stripe/stripe-js"
import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const CartPage = () => {
  const windowGlobal = typeof window !== 'undefined' && window
  const getStorageProducts = () => {
    var i,
      results = []
    var regex = "sku_"
    for (i in windowGlobal.localStorage) {
      if (windowGlobal.localStorage.hasOwnProperty(i)) {
        if (i.match(regex) || (!regex && typeof i === "string")) {
          var value = windowGlobal.localStorage.getItem(i)
          results.push({ key: i, val: value })
        }
      }
    }
    console.log(results)
    var cartContent = []
    for (i = 0; i < results.length; i++) {
      cartContent.push(JSON.parse(results[i].val))
    }
    return cartContent
  }

  const [cartProducts, updateCart] = useState(getStorageProducts)
  const stripePromise = loadStripe("pk_test_nn7tLZvXW7Qzq3h6idD1YnwY00A91LUWZU")

  const checkButton = async e => {
    e.preventDefault()
    const stripe = await stripePromise
    var skuList = []
    cartProducts.forEach(element => {
      var object = { sku: element.sku, quantity: element.quantity }
      skuList.push(object)
    })
    stripe
      .redirectToCheckout({
        items: skuList,
        successUrl: "https://clever-mclean-281753.netlify.app/checkout/success",
        cancelUrl: "https://clever-mclean-281753.netlify.app/checkout/cancel",
      })
      .then(function (result) {
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer
        // using `result.error.message`.
      })
  }

  const removeFromCart = sku => {
    windowGlobal.localStorage.removeItem(sku)
    updateCart(getStorageProducts)
  }

  const emptyCart = e => {
    e.preventDefault()
    var arr = []
    for (var i = 0; i < windowGlobal.localStorage.length; i++) {
      if (windowGlobal.localStorage.key(i).substring(0, 4) == "sku_") {
        arr.push(windowGlobal.localStorage.key(i))
      }
    }

    for (var i = 0; i < arr.length; i++) {
      windowGlobal.localStorage.removeItem(arr[i])
    }
    updateCart(getStorageProducts)
  }

  return (
    <Layout name="Cart">
      <div className="cart__grid">
        {cartProducts.map(element => (
          <div className="cart__item">
            <p>{element.title}</p>
            <p>{element.price} €</p>
            <img src={element.image} />
            <p>Quantity : {element.quantity}</p>
            <button onClick={() => removeFromCart(element.sku)}>
              Remove from the cart
            </button>
          </div>
        ))}
      </div>
      <button onClick={emptyCart}>Empty the cart</button>
      <button onClick={checkButton}>Pay</button>
    </Layout>
  )
}

export default CartPage
