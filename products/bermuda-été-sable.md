---
title: Bermuda été sable
price: 20,00
description: Bermuda été couleur sable pour hommes
image: /assets/short-sable-grande-taille-homme-duke-bermuda-coton-pas-cher-nouveaute.jpg
sku: sku_HMPa8ZGYLOkbqV
path: bermuda-sable
category:
  - bermuda
  - été
  - homme
attributes:
  - sable
---
