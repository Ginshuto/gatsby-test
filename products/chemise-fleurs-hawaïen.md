---
title: Chemise Fleurs Hawaïen
price: 30,00
description: Chemise noire imprimé fleurs style hawaïen pour hommes
image: /assets/71yzmjfkgdl._ac_ux385_.jpg
sku: sku_HMPWuJuTL9TzHi
path: chemise-noire
category:
  - chemise
  - été
  - homme
attributes:
  - noir
  - bleu
---
