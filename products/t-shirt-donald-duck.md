---
title: T-Shirt Donald Duck
price: 10,00
description: T-Shirt Gris avec un imprimé Donald Duck
image: /assets/se622o05q-c11-16.jpg
sku: sku_HMPgQuRrmiRC4S
path: tshirt-donald
category:
  - t-shirt
  - donald
attributes:
  - gris
  - imprimé
---
