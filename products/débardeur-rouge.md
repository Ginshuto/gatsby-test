---
title: Débardeur rouge
price: 20,00
description: Débardeur rouge Femme
image: /assets/walaka-dbardeur-femme-grande-taille-haut-femme-chic-chemise-sans-manche-tops-lgant-bandages-tee-shirt-tank-0-0.jpg
sku: sku_HMPigtNkfM14Lh
path: débardeur-rouge
category:
  - débardeur
  - femme
  - été
attributes:
  - rouge
---
