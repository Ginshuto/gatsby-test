---
title: Pantalon Rouge
price: 30,00
description: Pantalon rouge taillé homme
image: /assets/pantalon-chino-rouge-homme-teddy-smith.jpg
sku: sku_HMPiCqQA4LJO4N
path: pantalon-rouge
category:
  - pantalon
  - homme
attributes:
  - rouge
---
