---
title: Bermuda bleu-ciel
price: 20,00
description: Bermuda Bleu-ciel homme parfait pour l'été
image: /assets/ralph-lauren-short-bermuda-chino-ralph-lauren-bleu-ciel-pour-homme.jpg
sku: sku_HMPdorEptxjDPW
path: bermuda-bleu-ciel
category:
  - bermuda
  - été
  - homme
attributes:
  - bleu-ciel
---
