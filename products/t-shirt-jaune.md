---
title: T-shirt Jaune
price: 10,00
description: T-Shirt Jaune
image: /assets/tee-shirt-homme-cheryl-jaune.jpg
sku: sku_HMPe3BXsy29u93
path: tshirt-jaune
category:
  - t-shirt
attributes:
  - jaune
---
