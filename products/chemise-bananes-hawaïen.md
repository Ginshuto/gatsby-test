---
title: Chemise Bananes Hawaïen
price: 30,00
description: Chemise jaune imprimé bananes style hawaïen pour hommes
image: /assets/71gcs02hpll._ac_ux679_.jpg
sku: sku_HMPUqx7gI1zJVk
path: chemise-jaune
category:
  - chemise
  - été
  - homme
attributes:
  - jaune
---
