---
title: Bonnet Noir
price: 15,00
description: Bonnet noir en laine
image: /assets/813vh5rhy2l._ac_ux385_.jpg
sku: sku_HMPkKXXyz8qWwW
path: bonnet-noir
category:
  - bonnet
  - hiver
attributes:
  - noir
---
