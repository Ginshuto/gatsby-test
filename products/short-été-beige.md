---
title: Short Été Beige
price: 20,00
description: Short été couleur beige pour femmes
image: /assets/téléchargement-1-.jpg
sku: sku_HMPhWpk3L4aLl0
path: short-beige
category:
  - short
  - été
  - femme
attributes:
  - beige
---
